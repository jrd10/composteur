# Composteur

## Bonjour

Saint-André-les-Vergers, le mardi 6 juin 2023.

- J'ai créé ce projet pour partager des idées et des conceptions autour des composteurs et du compostage.

- Site en anglais à venir

## Le périmètre

### Pourquoi le compostage ?

- Le compostage s'inscrit dans une vision de l'économie circulaire. Il s'agit de « réduire nos "déchets" ».

- Ici, l'expression « réduire ses déchets » ne s'entend pas au sens qu'il ne faut plus rien faire. Le mot "déchet" ici est entendu au sens d'un statut. Quelque chose qui ne sera plus jamis utilisé et qui partira en décharge ou sera brûlé dans un incinérateur.

- Avec le compostage, par exemple, vos épluchures de fruits et légumes, après le compostage, retourneront à la terre et devienront des nutriments pour les plantations ultérieures. Le compostage est un exemple type d'économie circulaire.

## Différents familles de composteurs

- **Composteur individuel**. Que l'on place dans son jardin lorsque l'on dispose d'un espace privatif,

- **Composteur collectif**. Installé par une collectivité territoriale ou un groupe d'utilisateurs, à proximité d'un habitat collectif (copropriété, habitation à loyer modérée, village vacances...) et/ou d'un jardin collectif, permettant aux résidents de faire du compostage,

- **Composteur (autonome) d'établissement**. Composteur installé par, et pour, un établissement comme un hôpital, une maison de retraite, une cantine scolaire, etc. Ce composteur a un accès limité à la production des déchets de l'établissement en question.

Ces trois types de composteur, reposant sur le même principe du "compostage filière froide" (sauf pour les gros établissement), se présentent le plus simplement sous la forme d'une caisse, généralement en bois, posée à même sur le sol. Ils se distinguent par leur taille et des accessoires pour faciliter l'usage.

- Note : il existe d'autres système de compostage : composteur électromécanique, lombricomposteur... qui ne seront pas traités ici.


## Les différents produits

- Ici, et dans un premier temps, ce sont les composteurs individuels qui seront traités.

### Composteurs individuels

#### Le constat

- D'abord, je trouve un peu aberrant de voir des _composteurs individuels_ en plastique distribués par des collectivités territoriales dans une vision d'économie circulaire, zéro déchet. 

- Ensuite, je m'interroge aussi sur une distribution de composteur en kit, prêt à l'emploi, même en bois, pour les personnes en habitat individuel. Et le plus souvent avec un contribution de la collectivité qui peut aller de 50 % à 100 %. En effet, en habitat individuel, il est possible de "réserver" un coin de l'espace (d'environ un mètre-carré) délimité par quelques planches ou parpaings à même le sol.

- Car, il est aussi tout à fait possible et simple de fabriquer son propre composteur individuel avec des matériaux de récupération.
C'est ce qui est proposer comme alternative ci-dessous.

- Ce type de composteur est principalement destiné pour des biodéchets de cuisine de type "épluchures" ou des déchets verts de jardins. Il est possible d'aller plus en avant avec une certaine connaissance et expérience. 

- Le compost réalisé au bout d'une dizaine de mois à pour vocation :
    
    - de retourner dans ce même jardin, jardinières, pots de fleur, pour enrichir la terre 
    
    - ou pour du palliage dans les massifs de fleurs, autres.


#### Réaliser un composteur individuel avec des planches de récupération

- Il est proposé ici une alternative pour réaliser un composteur individuel simple avec des matériaux de récupération comme les planches d'une palette.

- N'hésiter pas à adapter le modèle selon vos besoins et les matériaux recyclés dont vous disposez.

- Au niveau de la taille d'un composteur, on peut imaginer quelque chose comme un cube de 0,8 à 1 m de côté.

- Ce composteur purrait être fabriqué :
    - soit-même avec :
        - une palette de récupération
        - ou des planches, préparées avec les encoches ou non, issues de ressourceries,
        
    - dans une ressourcerie, ou par une entreprise d'insertion, qui revendrait le produit clé en main, éventuellement avec une contribution de la collectivité. 

#### Documents 

- **Fichers FreeCAD (.FCStd)** et PDF, en français, dans le répertoire "Composteur individuel", en anglais, dans le répertoire "Individual Composter"

    - Note : j'ai un petit problème, lorsque j'ouvre un fichier .FCStd, les vues dans les pages ne s'affichent plus ! En attendant que je règle ce problème, voir les vues en PDF :).


## Licence

- En Open source Harware. Plan et idées dans le domaine plubic. CC0. Il n'est pas requis de citer cette source lors des réutilisations.

## Auteur et remerciements

- Vous pouvez facilement me contacter ici, sur GitLab.

- Mon compte Mastodon (alternative à Twitter) : https://framapiaf.org/@jose_relland

- Je remercie toutes les personnes qui ont contribué au développement de mes connaissances dans le compostage et à la maturation de ce projet. 
    - Et Plus particulièrement, le service Développement Durable et toutes les équipes et élus de Troyes Champagne Métropole, Zéro Déchet Troyes, le Réseau Compost Citoyen Grand Est, le Laboratoire Citoyen de la commune de Sainte-Savine, BioCyclade. 

- Je remercie la communauté FreeCAD pour la disponibilité du logiciel en open source : https://www.freecad.org/. Ainsi que le groupe FreeCAD sur LinkedIn : https://www.linkedin.com/groups/4295230/. 

- Et bien d'autres que je ne peux pas tous cités ici :).

## Roadmap
`À développer`

## contributions

- Je vous invite à améliorer les idées et modèles proposés ici. La citation de ce projet n'est pas requise.

- Je vous invite à forker le projet.

- Je vous invite à proposer vos références par des "merge request" dans le chapitre ci-dessous.


## Autres références

- Vos propositions et réalisations.


## Conclusion

- En avant vers une économie circulaire, vers le zéro déchet... vous vous rappelez ! Déchet en tant que "statut" :).
